## Simple Web Grid Layouts

### Each is Responsive down to 381px.

[www links of below](https://hamberfim.github.io/Simple_Web_Grid_Layouts/)


1.) <a href="https://hamberfim.github.io/Simple_Web_Grid_Layouts/wrap_n_stack/index.html" target="_blank">Wrap and Stack</a>

2.) <a href="https://hamberfim.github.io/Simple_Web_Grid_Layouts/cust_materialize/index.html" target="_blank">Customized Materialize</a>

3.) <a href="https://hamberfim.github.io/Simple_Web_Grid_Layouts/simple_hero/simple_hero.html" target="_blank">Simple Hereo CSS Grid</a>

4.) <a href="https://hamberfim.github.io/Simple_Web_Grid_Layouts/simple_hortz/index_hrzNav.html" target="_blank">Simple Horizontal Navigation (oldschool)</a> 

5.) <a href="https://hamberfim.github.io/Simple_Web_Grid_Layouts/simple_sidebar/index_sbNav.html" target="_blank">Simple Sidebar Navigation (oldschool)</a> 
